//This function call ThingML for compilation
var exec = require('child_process').exec;
var events = require('events');

module.exports = class DockerComposeEngine {
    constructor() {
        this.emitter = new events.EventEmitter();
    }

    deploy(node, config, msg) {
        var engine = this;
        var hasError = false;

        //TODO: fix shit
        // --log-level warning: docker-compose logs info message to standard error, which will be interpreted as an error here.
        //  nothing much we can do except hide info messages
        node.process = exec('docker-compose --log-level warning build && docker-compose up --abort-on-container-exit --exit-code-from $(docker-compose ps --services)', {
            cwd: msg.path
        });

        node.process.stdout.setEncoding('utf8');
        node.process.stdout.on('data', (data) => {
            data.trim().split('\n').forEach(line => {
                if (line.toLowerCase().startsWith('warning')) {
                    node.warn(line);
                } else if (line.toLowerCase().startsWith('error')) {
                    hasError = true;
                    node.error(line);
                } else {
                    node.log(line);
                }
            });
        });
        node.process.stderr.setEncoding('utf-8');
        node.process.stderr.on('data', (data) => {
            hasError = true;
            node.error(data);
        });

        node.process.on('error', (err) => {
            hasError = true;
            node.error('Could not deploy the containers. ' + err);
        });

        node.process.on('exit', (code) => {
            if (code !== 0) {
                hasError = true;
            }
            if (hasError) {
                node.error('Cannot complete because of errors!');
                if (engine.emitter.listenerCount('error') > 0) {
                    engine.emitter.emit('error');
                }
            } else {
                node.log('Done!');
                engine.emitter.emit('deployed');
            }
            delete node.process;
        });
    }

    close(node) {
        if (node.process) {
            node.process.kill();
        }
        delete node.process;
        //TODO: stop container
        this.emitter.removeAllListeners();
    };

    on(e, f) {
        this.emitter.on(e, f);
    }
}

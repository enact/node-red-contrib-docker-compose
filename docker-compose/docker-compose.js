module.exports = function(RED) {
    "use strict";
    var DockerComposeEngine = require("./docker-compose-engine");

    function DockerComposeNode(config) {
        RED.nodes.createNode(this, config);
        var node = this;
        var engine = new DockerComposeEngine();

        // Received message, trigger deploy
        //TODO: if we trigger build by message and the previous build didn't finish, what to do?
        node.on('input', function(msg) {
            node.status({fill:"yellow",shape:"dot",text:"docker-compose.status.deploying"});
            engine.deploy(node, config, msg.payload);
        });

        engine.on('deployed', function() {
            node.status({fill:"green",shape:"dot",text:"docker-compose.status.success"});
            var msg = { payload: config };
            node.send(msg);
        });

        engine.on('error', function(e) {
            node.status({fill:"red",shape:"ring",text:"node-red:common.status.error"});
            node.error(e);
        });

        // cleanup on flow stop
        node.on("close", function (done) {
            engine.close(node);
            done();
        });
    }
    RED.nodes.registerType("docker-compose", DockerComposeNode);
}
